import numpy as np
import seaborn as sns

np.random.seed(42)
data = np.random.normal(loc=0, scale=1, size=(100, 2))
labels = np.random.randint(0, 2, size=100)

class_0 = data[labels == 0]
class_1 = data[labels == 1]